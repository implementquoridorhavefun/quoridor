#include "pch.h"
#include "Player.h"

#include "../NewDll/PawnDll.h"
#include "../Logg/Logging.h"
#include <fstream>
#include<memory>



Player::Player(const std::shared_ptr<std::string> name, const uint8_t& availableWalls, const uint8_t& xCoord, const uint8_t& yCoord) :
	m_name(std::make_shared<std::string>(*name)), m_availabeWalls(availableWalls)
{
	Logg::loggFunction("Constructor for player, which have the name and the number of available walls. \n");
	

	m_pion.setCoordX(xCoord);
	m_pion.setCoordY(yCoord);
}


bool Player::setWall(const uint8_t& x, const uint8_t& y, Board<char> & board, sf::Vector2f origin, const bool& wallType)

{
	Logg::loggFunction("This method use the board, and the cells to set the wall on the board. And use another function to change the cells. \n");


	if (wallType == true)
	{
		if (board.setHorizontalWallToCell(x, y, origin))
		{
			--m_availabeWalls;
			return true;
		}
	}
	else if (board.setVerticalWallToCell(x, y, origin))
	{
		--m_availabeWalls;
		return true;
	}
	return false;
}


bool Player::movePawn(Board<char> & board,std::vector<std::pair<int,int>>& pawnPositions,bool& flag,const int& playerIndex) 

{
	Logg::loggFunction("This method move a pawn for a player.\n");
	
	auto ceva = std::find(pawnPositions.begin(), pawnPositions.end(), std::make_pair(m_pion.getCoordX(), m_pion.getCoordY()+1));
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{

		if (std::find(pawnPositions.begin(), pawnPositions.end(), std::make_pair(m_pion.getCoordX(), m_pion.getCoordY() + 1)) != pawnPositions.end())
		{
			if (!board.hasRightWall(m_pion.getCoordX(), m_pion.getCoordY()) && !board.hasRightWall(m_pion.getCoordX(), m_pion.getCoordY() + 1))
			{
				m_pion.setCoordY(m_pion.getCoordY() + 2);
				pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
				flag = true;
				std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
				std::cout << "right ";
				return true;
			}
			else if (board.hasRightWall(m_pion.getCoordX(), m_pion.getCoordY()) || board.hasRightWall(m_pion.getCoordX(), m_pion.getCoordY() + 1))
			{
				std::cout << "Invalid move! \n";
				return false;
			}
		}
		else if (!board.hasRightWall(m_pion.getCoordX(), m_pion.getCoordY()) )
		{
			m_pion.setCoordY(m_pion.getCoordY() + 1);
			pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
			flag = false;
			std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
			std::cout << "right ";
			return true;
		}
		else
			std::cout << "Invalid Move";
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{


		if (std::find(pawnPositions.begin(), pawnPositions.end(), std::make_pair(m_pion.getCoordX(), m_pion.getCoordY() -1)) != pawnPositions.end())
		{
			if (!board.hasLeftWall(m_pion.getCoordX(), m_pion.getCoordY()) && !board.hasLeftWall(m_pion.getCoordX(), m_pion.getCoordY() - 1))
			{
				m_pion.setCoordY(m_pion.getCoordY() - 2);
				pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
				flag = true;
				std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
				std::cout << "Left ";
				return true;
			}
			else if (board.hasLeftWall(m_pion.getCoordX(), m_pion.getCoordY()) || board.hasLeftWall(m_pion.getCoordX(), m_pion.getCoordY() - 1))
			{
				std::cout << "Invalid move! \n";
				return false;
			}
		}
		 else if (!board.hasLeftWall(m_pion.getCoordX(), m_pion.getCoordY()))

		{
			m_pion.setCoordY(m_pion.getCoordY() - 1);
			pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
			std::cout << "left ";
			std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
			return true;
		}
		else
			std::cout << "Invalid Move";
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{

		if (std::find(pawnPositions.begin(), pawnPositions.end(), std::make_pair(m_pion.getCoordX()-1, m_pion.getCoordY() )) != pawnPositions.end())

		{
			if (!board.hasUpperWall(m_pion.getCoordX(), m_pion.getCoordY()) && !board.hasUpperWall(m_pion.getCoordX()-1, m_pion.getCoordY() ))
			{
				m_pion.setCoordX(m_pion.getCoordX() - 2);
				pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
				flag = true;
				std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
				std::cout << "Up ";
				return true;
			}
			else if (board.hasUpperWall(m_pion.getCoordX(), m_pion.getCoordY()) || board.hasUpperWall(m_pion.getCoordX()-1, m_pion.getCoordY() ))
			{
				std::cout << "Invalid move! \n";
				return false;
			}
		}

		else if (!board.hasUpperWall(m_pion.getCoordX(), m_pion.getCoordY()))
		{
			m_pion.setCoordX(m_pion.getCoordX() - 1);
			pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
			std::cout << "UP";
			std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
			return true;
		}
		else
			std::cout << "Invalid Move";
	  
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{

		if (std::find(pawnPositions.begin(), pawnPositions.end(), std::make_pair(m_pion.getCoordX()+1, m_pion.getCoordY())) != pawnPositions.end())
		{
			if (!board.hasDownWall(m_pion.getCoordX(), m_pion.getCoordY()) && !board.hasDownWall(m_pion.getCoordX()+1, m_pion.getCoordY()))
			{
				m_pion.setCoordX(m_pion.getCoordX() + 2);
				pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
				flag = true;
				std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
				std::cout << "Down";
				return true;
			}
			else if (board.hasDownWall(m_pion.getCoordX(), m_pion.getCoordY()) || board.hasDownWall(m_pion.getCoordX()+1, m_pion.getCoordY() ))
			{
				std::cout << "Invalid move! \n";
				return false;
			}
		}

		 else if (!board.hasDownWall(m_pion.getCoordX(), m_pion.getCoordY()))
	{
			m_pion.setCoordX(m_pion.getCoordX() + 1);
			pawnPositions[playerIndex] = std::make_pair(m_pion.getCoordX(), m_pion.getCoordY());
			std::cout << "Down";
			std::cout << m_pion.getCoordX() << " " << m_pion.getCoordY();
			return true;
		}
		else std::cout << "Invalid Move";
	}
	return false;
}

bool Player::checkIfWeHaveAWinner(const int& playerIndex)
{
	Logg::loggFunction("checkIfWeHaveAWinner was called \n");

	switch (playerIndex)
	{
	case 0:
		if (m_pion.getCoordX() == 9)
			return true;
		else return false;
	case 1:
		if (m_pion.getCoordX() == 1)
			return true;
		else return false;
	case 2:
		if (m_pion.getCoordY() == 1)
			return true;
		else return false;
	case 3:
		if (m_pion.getCoordY() == 9)
			return true;
		else return false;
	default:
		return false;
	}

}

int Player::getWallNr() const
{
	return m_availabeWalls;
}
std::pair<int, int> Player::getXY() const
{
	return std::make_pair(m_pion.getCoordX(),m_pion.getCoordY());
}
std::string Player::getName() const
{
	return *m_name;
}
//