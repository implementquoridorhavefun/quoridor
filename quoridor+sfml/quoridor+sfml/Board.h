#pragma once
#include "Logg.h"
#include"pch.h"
#include "../NewDll/PawnDll.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include<vector>
#include<array>

template< typename T>
class Board
{
private:

	bool isSpeaceForHorizontalWall(const int x, const int y, sf::Vector2f origin);
	bool isSpeaceForVerticalWall(const int x, const int y, sf::Vector2f origin);
	bool verifyIfTheBitIsSetted(T var, const int  bit)const;
public:
	
	Board<T>();
	bool hasUpperWall(const int x, const int y)const;
	bool hasDownWall(const int x, const int y)const;
	bool hasRightWall(const int x, const int y)const;
	bool hasLeftWall(const int x, const int y)const;
	bool setVerticalWallToCell(const int x, const int y, sf::Vector2f origin);
	bool setHorizontalWallToCell(const int x, const int y, sf::Vector2f origin);
	

	void addWallOrigin(sf::Vector2f origin);
	std::vector<sf::Vector2f> getWallsOrigin();
	std::array < std::array<T, 11>, 11> getBoard();
	friend std::ostream& operator <<(std::ostream& os, Board board);

	
	
private:
	bool isOutOfBounds(const int x, const int y)const;
	void buildBorder();
private:
	std::vector<sf::Vector2f>m_wallsOrigins;
	static const int kBounds = 9;
	static const int kUp = 1 << 0;
	static const int kRight = 1 << 1;
	static const int kDown = 1 << 2;
	static const int kLeft = 1 << 3;
	std::array < std::array<T, 11>, 11>m_board;

};

template< typename T>
Board<T>::Board()
{
	Logg::loggFunction("Default constructor for board \n");


	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
			m_board[i][j] = NULL;

	}
	buildBorder();
}
template< typename T>
bool Board<T>::isSpeaceForHorizontalWall(const int x, const int y, sf::Vector2f origin)
{
	Logg::loggFunction("This method verify if we set the coordinates correct to put a wall, on coordinates x and y.The wall we set is horizontal. This method is also based on cells and the bits from cells, and verify them. \n ");


	if (isOutOfBounds(x - 1, y + 1) == false && isOutOfBounds(x, y) == false)
		if (hasUpperWall(x, y) == false && hasUpperWall(x, y + 1) == false && hasDownWall(x - 1, y) == false && hasDownWall(x - 1, y + 1) == false
			&& std::find(m_wallsOrigins.begin(), m_wallsOrigins.end(), origin) == m_wallsOrigins.end())
		{
			this->m_wallsOrigins.push_back(origin);
			return true;
		}
		else {
			return false;
			std::cout << "is not enough speace for wall";
		}
	else
	{
		throw std::out_of_range("Board index out of bound.");
		Logg::loggFunction("It is not enought speace for wall. \n");

	}
}
template< typename T>
bool Board<T>::isSpeaceForVerticalWall(const int x, const int y, sf::Vector2f origin)
{

	Logg::loggFunction("This method verify if we set the coordinates correct to put a wall, on coordinates x and y.The wall we set is vertical. This method is also based on cells and the bits from cells, and verify them. \n");

	if (isOutOfBounds(x - 1, y + 1) == false && isOutOfBounds(x, y) == false)
		if (hasRightWall(x, y) == false && hasLeftWall(x, y + 1) == false && hasRightWall(x - 1, y) == false && hasLeftWall(x - 1, y + 1) == false
			&& std::find(m_wallsOrigins.begin(), m_wallsOrigins.end(), origin) == m_wallsOrigins.end())
		{
			this->m_wallsOrigins.push_back(origin);
			return true;
		}
		else {
			return false;
			throw std::invalid_argument("out of bounds");
		}
	else
	{
		throw std::out_of_range("Board index out of bound.");
		Logg::loggFunction("It is not enought speace for wall. \n");
	}
}
template<typename T>
inline bool Board<T>::verifyIfTheBitIsSetted(T var, const int bit)const
{
	return (var & (bit)) ? true : false;
}

template< typename T>
void Board<T>::addWallOrigin(sf::Vector2f origin)
{
	Logg::loggFunction("Add in an vector of origins the center of a wall which is set on board by a player. \n");


	this->m_wallsOrigins.push_back(origin);
}
template< typename T>
std::vector<sf::Vector2f> Board<T>::getWallsOrigin()
{
	Logg::loggFunction("Get wall origins fuction was called \n");
	return this->m_wallsOrigins;
}
template< typename T>
std::array<std::array<T, 11>, 11> Board<T>::getBoard()
{
	Logg::loggFunction("Get boredr was called \n");
	return this->m_board;
}
template< typename T>
bool Board<T>::isOutOfBounds(const int x, const int y) const
{
	Logg::loggFunction("This method verify if the coordinates where we will set wall are correct and belongs to board, and the wall is not set outside of the board. \n");

	if (x < 1 || x > kBounds || y < 1 || y > kBounds)
		return true;
	return false;
}
template< typename T>
bool Board<T>::hasUpperWall(const int x, const int y)const
{
	Logg::loggFunction("This method verify if a position on board have an upper wall. This method use bits which are stored in cells, if the bit from up cell is 1 we have an upper wall. \n ");

	if (verifyIfTheBitIsSetted(this->m_board[x][y], kUp)) return true;
	return false;
}

template< typename T>
bool Board<T>::hasDownWall(const int x, const int y) const
{
	Logg::loggFunction("This method verify if a position on board have an down wall. This method use bits which are stored in cells, if the bit from down cell is 1 we have an down wall. \n");


	if (verifyIfTheBitIsSetted(this->m_board[x][y], kDown)) return true;
	return false;
}

template< typename T>
bool Board<T>::hasRightWall(const int x, const int y)const
{
	Logg::loggFunction("This method verify if a position on board have a right wall. This method use bits which are stored in cells, if the bit from right cell is 1 we have an right wall. \n");


	if (verifyIfTheBitIsSetted(this->m_board[x][y] , kRight)) return true;
	return false;
}
template< typename T>
bool Board<T>::hasLeftWall(const int x, const int y)const
{
	Logg::loggFunction("This method verify if a position on board have a left wall. This method use bits which are stored in cells, if the bit from left cell is 1 we have an left wall. \n");


	if (verifyIfTheBitIsSetted(this->m_board[x][y] ,kLeft)) return true;
	return false;
}

template< typename T>
bool Board<T>::setVerticalWallToCell(const int x, const int y, sf::Vector2f origin)
{
	Logg::loggFunction("Try to change the bits of cells after we set a vertical wall on board, in order to remember that we have a wall. \n");


	if (isSpeaceForVerticalWall(x, y, origin))
	{
		this->m_board[x][y] |= ( kRight);
		this->m_board[x - 1][y] |= ( kRight);
		this->m_board[x][y + 1] |= (kLeft);
		this->m_board[x - 1][y + 1] |= (kLeft);
		return true;
	}

	return false;

}
template< typename T>
bool Board<T>::setHorizontalWallToCell(const int x, const int y, sf::Vector2f origin)

{
	Logg::loggFunction("This method will change the bits of cells after we set a horizontal wall on board, in order to remember that we have a wall. \n");

	if (isSpeaceForHorizontalWall(x, y, origin) == true)
	{
		this->m_board[x][y] |= (kUp);
		this->m_board[x - 1][y] |= (kDown);
		this->m_board[x][y + 1] |= (kUp);
		this->m_board[x - 1][y + 1] |= (kDown);
		return true;
	}
	return false;
}

template< typename T>
std::ostream & operator<<(std::ostream & os, Board<T> board)
{
	Logg::loggFunction("This method overload the '<<' operator and return the board. \n");


	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
			os << board.m_board[i][j] << " ";
		os << " \n";
	}
	return os;
}
template< typename T>
void Board<T>::buildBorder()
{
	Logg::loggFunction("Border was built \n");


	
	for (int j = 0; j <= kBounds; j++)
	{
		//builds up border
		m_board[0][j] |= (kDown);
		m_board[1][j] |= (kUp);

		//builds down border
		m_board[10][j] |= (kUp);
		m_board[9][j] |= (kDown);

		//builds left boredr
		m_board[j][0] |= ( kRight);
		m_board[j][1] |= (kLeft);

		//builds right border
		m_board[j][10] |= (kLeft);
		m_board[j][9] |= ( kRight);
	}

	
}
