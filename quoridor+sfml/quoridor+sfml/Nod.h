#pragma once
#include<iostream>
#include <map>
#include<string>
#include <vector>

using namespace std;
using linCol = pair<int, int>;

class Nod
{
public:
	Nod();
	Nod(const int& linie, const int& coloana);
	Nod(const Nod& other);
	Nod(Nod && other);

	void addVecini(string, Nod);
	void deleteVecin(string);
	int getFirst() const;
	int getSecond() const;
	vector<Nod> getVecini() const;
	bool operator ==(const Nod& other) const;

	Nod& operator = (const Nod& other);
	Nod& operator = (Nod&& other);

	friend std::ostream& operator << (std::ostream& os, const Nod& nod);
private:
	linCol m_indiceNod;
	std::map<string,Nod> m_vecini;
};

