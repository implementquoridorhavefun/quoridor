#pragma once
#include "../NewDll/PawnDll.h"
#include <SFML/Graphics.hpp>
#include "Player.h"
#include <Array>
#include <Vector>
#include <iostream>
#include "Logg.h"
#include "AIUtility.h"

class GameGraphics
{
public:
	GameGraphics();

	void open();
	void intro();
	void openFor2();
	void winMessage(const std::string&);
	void moveGraphicsFor2(int&, Player&, Board<char>&);
	void windowFor2(sf::RenderWindow&,const int&);
	void playersName(std::shared_ptr<std::string> name);
	void wallPlaceGraphics(int x, int y, int&, int&, sf::RenderWindow&, sf::Event&);
	void openFor4();
	void verticalWallPlace(const float& x,const float& y, int&);
	void horizontalWallPlace(const float& x,const float& y, int&);

public:

	using Position = std::pair<int, int>;
private:
	void buildingPieces(sf::Texture&);
private:
	static const uint8_t kMarjaDeEroare = 1;
	static const size_t kNumberOfRowsAndColumns = 9;
	static const uint8_t kSetupForTwo = 2;
	static const uint8_t kSetupForFour = 4;
	static const uint8_t kTopX = 0;
	static const uint8_t kTopY = 4;
	static const uint8_t kBottomX = 8;
	static const uint8_t kBottomY = 4;
	static const uint8_t kLeftX = 4;
	static const uint8_t kLeftY = 0;
	static const uint8_t kRightX = 4;
	static const uint8_t kRightY = 8;
private:

	Board<char>m_board;

	std::vector<Position> pawnPosition;
	
	std::vector<Player> m_players;
	std::array<sf::RectangleShape, 20> m_wallVector;
	std::vector<sf::RectangleShape> m_pieces;
	std::vector<sf::RectangleShape> m_grid;

	const sf::Vector2f gridSize = sf::Vector2f(60, 60);

	AIUtility m_AI;
};

