#include "stdafx.h"
#include "CppUnitTest.h"
#include <SFML/Graphics.hpp>
#include "..\Logg\Logging.h"
#include "Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuartoTests
{
	TEST_CLASS(BoardTest)
	{
	public:


		TEST_METHOD(TestHasRightWall)
		{
			Board board;
			board.SetVerticalWallToCell(5, 5, sf::Vector2f(120, 5.f));
			Assert::IsTrue(board.HasRightWall(5, 5));
		}

		TEST_METHOD(TestHasRightWallNoWall)
		{
			Board board;
			Assert::IsFalse(board.HasRightWall(5, 5));
		}
		TEST_METHOD(TestHasLeftWall)
		{
			Board board;
			board.SetVerticalWallToCell(5, 5, sf::Vector2f(120, 5.f));
			Assert::IsTrue(board.HasLeftWall(5, 6));
		}
		TEST_METHOD(TestHasLeftWallNoWall)
		{
			Board board;
			Assert::IsFalse(board.HasLeftWall(5, 6));
		}
		TEST_METHOD(TestHasDownWallNoWall)
		{
			Board board;
			Assert::IsFalse(board.HasDownWall(5, 5));
		}
	};
}