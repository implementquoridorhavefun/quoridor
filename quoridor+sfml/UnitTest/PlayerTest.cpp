#include "stdafx.h"
#include "CppUnitTest.h"

#include "..\NewDll\PawnDll.h"
#include "Player.h"
#include "Board.h"
#include <iostream>
#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuartoTests
{
	TEST_CLASS(PlayerTest)
	{
	public:

		TEST_METHOD(DefaultConstructorAndCheckIfXCoordModified)
		{
			Pawn pawn;
			Player player();
			int x1 = pawn.getCoordX();
			Assert::IsTrue(x1 !=5 );
			int y1 = pawn.getCoordY();
			Assert::IsTrue(y1!=5);
			
		}

		
		TEST_METHOD(MovePawnRight)
		{
			Pawn pawn;
			Board<char> board;
			pawn.setCoordY(5);
			pawn.setCoordX(5);
			board.setVerticalWallToCell(5, 5, sf::Vector2f(120, 5.f));
			Assert::IsTrue(board.hasRightWall(pawn.getCoordX(), pawn.getCoordY()));
		}

		TEST_METHOD(MovePawnLeft)
		{
			Pawn pawn;

			Board<char> board;
			pawn.setCoordY(5);
			pawn.setCoordX(5);
			board.setVerticalWallToCell(5, 5, sf::Vector2f(120, 5.f));
			if (board.hasLeftWall(pawn.getCoordX(), pawn.getCoordY() == true))

				Assert::Fail();
		}

		TEST_METHOD(MovePawnUP)
		{
			Pawn pawn;

			Board<char> board;
			pawn.setCoordY(5);
			pawn.setCoordX(5);
			board.setVerticalWallToCell(5, 5, sf::Vector2f(120, 5.f));
			if (board.hasUpperWall(pawn.getCoordX(), pawn.getCoordY() == true))

				Assert::Fail();
		}

		TEST_METHOD(MovePawnDown)
		{
			Pawn pawn;

			Board<char> board;	
			pawn.setCoordY(5);
			pawn.setCoordX(5);
			board.setVerticalWallToCell(5, 5, sf::Vector2f(120, 5.f));
			if (board.hasDownWall(pawn.getCoordX(), pawn.getCoordY() == true))

				Assert::Fail();
		}

	};
}