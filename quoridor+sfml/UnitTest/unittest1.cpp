#include "stdafx.h"
#include "CppUnitTest.h"
#include"board.h"
#include <SFML/Graphics.hpp>



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(BoardUnitTest)
	{
	private: const int coordX = 5, coordY = 5;
	public:

		TEST_METHOD(TestHasRightWall)
		{
			Board<char> board;
			board.setVerticalWallToCell(coordX, coordY, sf::Vector2f(120, 5.f));
			Assert::IsTrue(board.hasRightWall(coordX, coordX));
		}

		TEST_METHOD(TestHasRightWallNoWall)
		{
			Board<char> board;
			Assert::IsFalse(board.hasRightWall(coordX, coordY));
		}
		TEST_METHOD(TestHasLeftWall)
		{
			Board<char> board;
			board.setVerticalWallToCell(coordX, coordX, sf::Vector2f(120, 5.f));
			Assert::IsTrue(board.hasLeftWall(coordX, coordY + 1));
		}
		TEST_METHOD(TestHasLeftWallNoWall)
		{
			Board<char> board;
			Assert::IsFalse(board.hasLeftWall(coordX, coordY + 1));
		}
		TEST_METHOD(TestHasDownWall)
		{
			Board<char> board;
			board.setHorizontalWallToCell(coordX, coordY , sf::Vector2f(120, 5.f));
			Assert::IsTrue(board.hasDownWall(coordX- 1 , coordY));
		}
		TEST_METHOD(TestHasDownWallNoWall)
		{
			Board<char> board;
			Assert::IsFalse(board.hasDownWall(coordX, coordY ));
		}
		TEST_METHOD(TestHasUpperWall)
		{
			Board<char> board;
			board.setHorizontalWallToCell(coordX, coordY , sf::Vector2f(120, 5.f));
			Assert::IsTrue(board.hasUpperWall(coordX, coordY ));
		}
		TEST_METHOD(TestHasUpperWallNoWall)
		{
			Board<char> board;
			Assert::IsFalse(board.hasUpperWall(coordX, coordY));
		}
		TEST_METHOD(TestAddWallOrigin)
		{
			Board<char> board;
			board.addWallOrigin(sf::Vector2f(120, 5.f));
			std::vector<sf::Vector2f> wallsOrigins = board.getWallsOrigin();
			int WallsNumberOnBoard = wallsOrigins.size();
			Assert::AreEqual(WallsNumberOnBoard, 1);
		}
		TEST_METHOD(TestDefaultConstructor)
		{
			Board<char> board;
			std::array < std::array<char, 10>, 10>myArray;
			for (int i = 2; i < 9; i++)
			{
				for (int j = 2; j < 0; j++)
				{
					myArray[i][j] = NULL;
					Assert::AreEqual(myArray[i][j], board.getBoard()[i][j]);
				}

			}
		}
		TEST_METHOD(ExpectCallThrowExceptionSetHorizontalWallToCell)
		{
			Board<char> board;
			bool throwException = false;
			try { board.setHorizontalWallToCell(coordX +5, coordY + 7, sf::Vector2f(120, 5.f)); }
			catch (std::out_of_range) { throwException = true; }
			Assert::IsTrue(throwException);

		}
		TEST_METHOD(ExpectCallThrowExceptionSetVerticalWallToCell)
		{
			Board<char> board;
			bool throwException = false;
			try { board.setVerticalWallToCell(coordX + 5, coordY + 7, sf::Vector2f(120, 5.f)); }
			catch (std::out_of_range) { throwException = true; }
			Assert::IsTrue(throwException);

		}
		TEST_METHOD(TestSetVerticalWallToCell)
		{
			Board<char> board;
			board.setVerticalWallToCell(coordX, coordY, sf::Vector2f(120, 5.f));
			std::vector<sf::Vector2f> wallsOrigins = board.getWallsOrigin();
			int wallsNumberOnBoard = wallsOrigins.size();

			Assert::IsTrue(board.hasRightWall(coordX, coordY));
			Assert::IsTrue(board.hasRightWall(coordX-1, coordY));
			Assert::IsTrue(board.hasLeftWall(coordX-1, coordY+1));
			Assert::IsTrue(board.hasLeftWall(coordX, coordY+1));
			Assert::AreEqual(wallsNumberOnBoard, 1);

		}
		TEST_METHOD(TestSetHorizontalWallToCell)
		{
			Board<char> board;
			board.setHorizontalWallToCell(coordX, coordY, sf::Vector2f(120, 5.f));
			std::vector<sf::Vector2f> wallsOrigins = board.getWallsOrigin();
			int wallsNumberOnBoard = wallsOrigins.size();

			Assert::IsTrue(board.hasUpperWall(coordX, coordY));
			Assert::IsTrue(board.hasDownWall(coordX - 1, coordY));
			Assert::IsTrue(board.hasDownWall(coordX - 1, coordY + 1));
			Assert::IsTrue(board.hasUpperWall(coordX, coordY + 1));
			Assert::AreEqual(wallsNumberOnBoard, 1);

		}

	};
}