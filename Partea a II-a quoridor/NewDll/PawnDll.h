#pragma once
//#include "pch.h"

#include <iostream>

#ifdef NEWDLL_EXPORTS
#define LOGGING_API __declspec(dllexport)
#else
#define LOGGING_API __declspec(dllimport)
#endif

class LOGGING_API Pawn
{
public:
	 enum class Color : uint8_t
	{
		Red,
		Blue,
		Green,
		Yellow
	};
public:
	  Pawn();
	  Pawn(Color color);

	  Pawn(const Pawn& other);
	  Pawn(Pawn&& other);

	  Pawn& operator=(const Pawn& other);
	  Pawn& operator= (Pawn&& other);


	  ~Pawn();

      Color GetColor() const;

	  friend std::ostream& operator << (std::ostream& os, const Pawn& pawn);


	  int getCoordX() const;
	  int getCoordY() const;

	  void setCoordX(const float& pawn);
	  void setCoordY(const float& pawn);


private:
	 Color m_color;

	 int m_coordX;
     int m_coordY;
};