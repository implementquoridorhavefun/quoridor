//#include "pch.h"
//#include "../Logg/Logging.h"
#include "PawnDll.h"
#include <fstream>


Pawn::Pawn(Color color) :
	m_color(color)
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("Constructor default pawn", Logger::Level::Info);
	of.close();*/
}

Pawn::Pawn(const Pawn & other)
{
	*this = other;
}

Pawn::Pawn(Pawn && other)
{
	*this = std::move(other);
}

Pawn::Color Pawn::GetColor() const
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("This method get the color of a pawn", Logger::Level::Info);
	of.close();*/

	return m_color;
}

Pawn::Pawn()
{
}

Pawn& Pawn::operator=(const Pawn& other)
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("This method overload '=' operator so we can set a color of a pawn with another pawn color", Logger::Level::Info);
	of.close();*/

	m_color = other.m_color;
	m_coordX = other.m_coordX;
	m_coordY = other.m_coordY;

	return *this;
}

Pawn & Pawn::operator=(Pawn && other)
{
	m_color = other.m_color;
	m_coordX = other.m_coordX;
	m_coordY = other.m_coordY;

	new(&other) Pawn;

	return *this;
}

Pawn::~Pawn()
{
}

std::ostream& operator<<(std::ostream& os, const Pawn& pawn)
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("This method return the color of a pawn", Logger::Level::Info);
	of.close();*/

	return os << pawn.m_color;
}

int Pawn::getCoordX() const
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("This method get the x coordinate of a pawn", Logger::Level::Info);
	of.close();*/
	return m_coordX;
}

int Pawn::getCoordY() const
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("This method get the y coordinate of a pawn", Logger::Level::Info);
	of.close();*/

	return m_coordY;
}

void Pawn::setCoordX(const float& pawn)
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("This method set the x coordinate of a pawn", Logger::Level::Info);
	of.close();*/
	this->m_coordX = pawn;
}

void Pawn::setCoordY(const float& pawn)
{
	/*std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("This method set the y coordinate of a pawn", Logger::Level::Info);
	of.close();*/

	this->m_coordY = pawn;
}
