#include "pch.h"
#include "Logg.h"

void Logg::loggFunction(const std::string & message)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log(message, Logger::Level::Info);
	of.close();
}
