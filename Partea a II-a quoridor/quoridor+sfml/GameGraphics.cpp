#include "pch.h"
#include "GameGraphics.h"
#include <fstream>
#include "../Logg/Logging.h"


GameGraphics::GameGraphics()

{
	Logg::loggFunction("Constructor for game graphics. \n");
	

	for (int i = 0; i < m_wallVector.size(); ++i)
	{
		sf::RectangleShape Wall(sf::Vector2f(115, 5.f));
		Wall.setFillColor(sf::Color::Red);
		m_wallVector[i] = Wall;
	}

	//loading textures
	sf::Texture player;
	player.loadFromFile("pawn-64.png");
	buildingPieces(player);
	buildingPieces(player);
	buildingPieces(player);
	buildingPieces(player);

	//loading board ;
	for (int x = 0; x < kNumberOfRowsAndColumns; x++)
	{
		for (int y = 0; y < kNumberOfRowsAndColumns; y++)
		{
			sf::RectangleShape cell(gridSize);
			cell.setFillColor(sf::Color::Green);
			cell.setOutlineColor(sf::Color::White);
			cell.setOutlineThickness(5.0f);
			cell.setPosition(gridSize.x * x + 100, gridSize.y * y + 100);
			m_grid.push_back(cell);
		}
	}
	m_AI.createGraf(m_board);
}

void GameGraphics::open()
{
	Logg::loggFunction("This method open the window for Quoridor game. \n");

	sf::VideoMode videoMode = sf::VideoMode(1000, 700);
	sf::RenderWindow Window(videoMode, "Quoridor");
	Window.setFramerateLimit(30);
	sf::Event event;
	std::shared_ptr<std::string> sir;


	while (Window.isOpen())
	{
		while (Window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				Window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
				playersName(sir);


		}
		Window.clear();

		for (auto line : m_grid)
		{
			Window.draw(line);
		}
		Window.display();

	}
}

void GameGraphics::intro()
{
	Logg::loggFunction("This method will open the intro for the game where we will select the number of the players. And sets the text font, dimension, color and underline. If we press key 2 the game will open for 2 players, if we press key 4 the game will open for 4 players \n");


	sf::VideoMode videoMode = sf::VideoMode(500, 500);
	sf::RenderWindow Window(videoMode, "Quoridor");
	Window.setFramerateLimit(30);
	sf::Event event;

	sf::Text text;
	sf::Font font;
	font.loadFromFile("font.otf");

	text.setFont(font);

	text.setString("Pleas select the number \n of players by  presing \n the wanted number :\n\n\n\n 2 Players \n\n\n\n 4 Players");

	text.setCharacterSize(24);

	text.setFillColor(sf::Color::Red);

	text.setStyle(sf::Text::Bold | sf::Text::Underlined);




	while (Window.isOpen())
	{

		while (Window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				Window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
			{
				Window.close();
				openFor2();
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
			{
				Window.close();
				open();
			}
		}
		Window.clear();
		Window.draw(text);

		Window.display();
	}
}

void GameGraphics::openFor2()
{
	Logg::loggFunction("Open and display the game for 2 players. Sets the textures of pawns and color of board. If the player press key 1, the player will move the pawn.If the player will press key 2, the player will write on console the coordinates where he wants to set a wall. \n");


	sf::VideoMode videoMode = sf::VideoMode(1000, 700);
	sf::RenderWindow Window(videoMode, "Quoridor");
	Window.setFramerateLimit(30);
	sf::Event event;
	std::shared_ptr<std::string> name= (std::make_shared<std::string>(""));
	Window.setKeyRepeatEnabled(false);

	sf::Texture texture1;
	texture1.loadFromFile("pawn-64.png");
	m_pieces[0].setTexture(&texture1);
	m_pieces[0].setPosition(gridSize.x * kTopY + 100, gridSize.y * kTopX + 100);
	playersName(name);
	m_players.emplace_back(name, m_wallVector.size() / kSetupForTwo, kTopX + kMarjaDeEroare, kTopY + kMarjaDeEroare);
	pawnPosition.emplace_back(kTopX + kMarjaDeEroare, kTopY + kMarjaDeEroare);

	std::cout << *name << "\n" << kTopX << " " << kTopY;

	name = (std::make_shared<std::string>(""));

	sf::Texture texture2;
	texture2.loadFromFile("pawn-51.png");
	m_pieces[1].setTexture(&texture2);
	m_pieces[1].setPosition(gridSize.x * kBottomY + 100, gridSize.y * kBottomX + 100);
	playersName(name);
	m_players.emplace_back(name, m_wallVector.size() / kSetupForTwo, kBottomX + kMarjaDeEroare, kBottomY + kMarjaDeEroare);
	pawnPosition.emplace_back(kBottomX + kMarjaDeEroare, kBottomY + kMarjaDeEroare);

	std::cout << *name << "\n" << kBottomX << " " << kBottomY << " ";
	std::cout << m_wallVector.size() / kSetupForTwo;
	

	sf::Text nameView;
	sf::Font font;

	font.loadFromFile("font.otf");
	nameView.setFont(font);
	nameView.setCharacterSize(10);
	nameView.setFillColor(sf::Color::Red);
	nameView.setStyle(sf::Text::Bold);
	nameView.setPosition(280, 30);

	int playersIndex = 0;
	int wallIndex = 0;



	while (Window.isOpen())
	{
		
		while (Window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				Window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
			{
				std::cout << playersIndex << "\n";
				moveGraphicsFor2(playersIndex, m_players[playersIndex], m_board);
				std::cout << playersIndex << "\n";
				if (playersIndex < 1)
					if (m_players[1].checkIfWeHaveAWinner(1))
					{
						Window.close();
						winMessage(m_players[1].getName());
					}
				if (playersIndex == 1)
					if (m_players[0].checkIfWeHaveAWinner(0))
					{
						Window.close();
						winMessage(m_players[0].getName());
					}
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
			{
				if (m_players[playersIndex].getWallNr() != 0)
					wallPlaceGraphics(0, 0, wallIndex, playersIndex, Window, event);
				else std::cout << " you are out of walls\n";
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
			{
				bool flag;
				if (playersIndex == 0)
					flag = true;
				else flag = false;
				m_AI.hinturi(m_players[playersIndex].getXY(), m_players[m_players.size() - 1 - playersIndex].getXY(), flag);
			}

		}
		windowFor2(Window,playersIndex);
		Window.draw(nameView);
	}
}

void GameGraphics::winMessage(const std::string& name)
{

	Logg::loggFunction("win message was printed \n");

	sf::VideoMode videoMode = sf::VideoMode(500, 100);
	sf::RenderWindow Window(videoMode, "Quoridor");
	Window.setFramerateLimit(30);
	sf::Event event;

	sf::Text text;
	sf::Text showName;
	sf::Font font;
	font.loadFromFile("font.otf");

	text.setFont(font);
	showName.setFont(font);

	text.setString("The winner is : \n\n\n");

	text.setCharacterSize(24);
	showName.setCharacterSize(24);

	text.setFillColor(sf::Color::Red);
	showName.setFillColor(sf::Color::Red);

	text.setStyle(sf::Text::Bold | sf::Text::Underlined);


	while (Window.isOpen())
	{

		while (Window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				Window.close();
		}
		showName.setString(name);
		showName.setPosition(0, 50);
		Window.clear();
		Window.draw(text);
		Window.draw(showName);

		Window.display();
	}
}

void GameGraphics::moveGraphicsFor2(int&i, Player& player, Board<char>& board)
{
	Logg::loggFunction("This method will make the two pawns to move in direction indicated by keyboard arrows. \n");
	bool flag = false;
	while (true)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			if (player.movePawn(board,pawnPosition,flag,i))
			{
				if (flag == true)
				{
					m_pieces[i].move(-gridSize.x*2, 0);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
				else
				{
					m_pieces[i].move(-gridSize.x , 0);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			if (player.movePawn(board, pawnPosition, flag,i))
			{
				if (flag == true)
				{
					m_pieces[i].move(gridSize.x*2, 0);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
				else 
				{
					m_pieces[i].move(gridSize.x , 0);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			if (player.movePawn(board, pawnPosition, flag,i))
			{
				if (flag == true)
				{
					m_pieces[i].move(0, -gridSize.y*2);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
				else 
				{
					m_pieces[i].move(0, -gridSize.y);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			if (player.movePawn(board, pawnPosition, flag,i))
			{
				if (flag == true)
				{
					m_pieces[i].move(0, gridSize.y*2);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
				else
				{
					m_pieces[i].move(0, gridSize.y);
					if (i < 1)
						++i;
					else
						--i;
					return;
				}
			}
		}
	}
}

//putem face aceasta functi universala prin adaugarea unui int (care v-a fii constanta de definire a playerilor )
void GameGraphics::windowFor2(sf::RenderWindow &Window,const int& playersIndex)
{
	Window.clear();
	sf::Text nameView;
	sf::Font font;
	sf::Text message;
	sf::Text wallMessage;
	sf::Text wallNr;
	sf::Text help;
	sf::Text help1;
	sf::Text help2;
	sf::Text help3;

	font.loadFromFile("font.otf");
	nameView.setFont(font);
	nameView.setCharacterSize(15);
	nameView.setFillColor(sf::Color::Red);
	nameView.setStyle(sf::Text::Bold);
	nameView.setPosition(700, 50);
	nameView.setString(m_players[playersIndex].getName());

	message.setFont(font);
	message.setCharacterSize(15);
	message.setFillColor(sf::Color::Red);
	message.setStyle(sf::Text::Bold);
	message.setPosition(700, 30);
	message.setString("Turn : ");

	wallMessage.setFont(font);
	wallMessage.setCharacterSize(15);
	wallMessage.setFillColor(sf::Color::Red);
	wallMessage.setStyle(sf::Text::Bold);
	wallMessage.setPosition(700, 70);
	wallMessage.setString("Number of walls : ");
	
	
	wallNr.setFont(font);
	wallNr.setCharacterSize(15);
	wallNr.setFillColor(sf::Color::Red);
	wallNr.setStyle(sf::Text::Bold);
	wallNr.setPosition(700, 90);
	wallNr.setString(std::to_string(m_players[playersIndex].getWallNr()));

	help.setFont(font);
	help.setCharacterSize(11);
	help.setFillColor(sf::Color::Red);
	help.setStyle(sf::Text::Bold);
	help.setPosition(700, 410);
	help.setString("press key '1' to move the pawn; ");

	help1.setFont(font);
	help1.setCharacterSize(11);
	help1.setFillColor(sf::Color::Red);
	help1.setStyle(sf::Text::Bold);
	help1.setPosition(700, 430);
	help1.setString( " press key '2' to add a wall");

	help2.setFont(font);
	help2.setCharacterSize(11);
	help2.setFillColor(sf::Color::Red);
	help2.setStyle(sf::Text::Bold);
	help2.setPosition(700, 450);
	help2.setString("then press 'b' for vertical wall");

	help3.setFont(font);
	help3.setCharacterSize(11);
	help3.setFillColor(sf::Color::Red);
	help3.setStyle(sf::Text::Bold);
	help3.setPosition(700, 470);
	help3.setString("or 'n' for horizontal wall");


	/*wallMessage.setPosition(700, 130);
	wallMessage.setString("1 if you want to move your pawn / n ");
	wallMessage.setPosition(700, 150);
	wallMessage.setString("2 if you want to pleace a wall(b for vertical wall and n for horizontal wall).");
*/

	for (auto line : m_grid)
	{
		Window.draw(line);
	}
	for (int i = 0; i < m_wallVector.size(); ++i)
		Window.draw(m_wallVector[i]);
	Window.draw(nameView);
	Window.draw(help);
	Window.draw(help1);
	Window.draw(help2);
	Window.draw(help3);
	Window.draw(message);
	Window.draw(wallMessage);
	Window.draw(wallNr);
	Window.draw(m_pieces[0]);
	Window.draw(m_pieces[1]);
	Window.display();
}

void GameGraphics::playersName(std::shared_ptr<std::string> name)
{
	Logg::loggFunction("Player name was set! \n");
	

	
	sf::VideoMode videoMode = sf::VideoMode(500, 100);
	sf::RenderWindow Window(videoMode, "Quoridor");
	Window.setFramerateLimit(30);
	sf::Event event;

	sf::Text text;
	sf::Text showName;
	sf::Font font;
	font.loadFromFile("font.otf");

	text.setFont(font);
	showName.setFont(font);

	text.setString("Pleas enter a Name : \n\n\n");

	text.setCharacterSize(24);
	showName.setCharacterSize(24);

	text.setFillColor(sf::Color::Red);
	showName.setFillColor(sf::Color::Red);

	text.setStyle(sf::Text::Bold | sf::Text::Underlined);


	while (Window.isOpen())
	{

		while (Window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				Window.close();
			if (event.type == sf::Event::TextEntered)
				if (event.text.unicode < 128)
					*name += static_cast<char>(event.text.unicode);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
				return;

		}
		showName.setString(*name);
		showName.setPosition(0, 50);
		Window.clear();
		Window.draw(text);
		Window.draw(showName);

		Window.display();
	}
}

void GameGraphics::wallPlaceGraphics(int x, int y, int & wallIndex, int& playerIndex, sf::RenderWindow & Window, sf::Event& evn)
{

	Logg::loggFunction("Wall place graphics was called \n");
	


	m_wallVector[wallIndex].setPosition(gridSize.x * x + +100, gridSize.y * y + 95);

	bool wallTypeIndicator = true;


	while (Window.isOpen())
	{
		while (Window.pollEvent(evn))
		{
			if (evn.type == sf::Event::Closed)
				Window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{

				{
					m_wallVector[wallIndex].move(-gridSize.x, 0);
					--x;
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				m_wallVector[wallIndex].move(gridSize.x, 0);
				++x;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{

				m_wallVector[wallIndex].move(0, -gridSize.y);
				--y;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				m_wallVector[wallIndex].move(0, gridSize.y);
				++y;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
			{
				if (wallTypeIndicator == true)
				{
					m_wallVector[wallIndex].rotate(-90);
					m_wallVector[wallIndex].setPosition(gridSize.x * (x + kMarjaDeEroare) + 100, gridSize.y * (y + kMarjaDeEroare) + 95);
					std::cout << " Vertical\n";
					wallTypeIndicator = false;
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))
				if (wallTypeIndicator == false)
				{
					m_wallVector[wallIndex].rotate(90);
					m_wallVector[wallIndex].setPosition(gridSize.x * x + +100, gridSize.y * y + 95);
					std::cout << " Horizontal\n";
					wallTypeIndicator = true;
				}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
			{
				switch (wallTypeIndicator)
				{
				case true:
					try
					{

						if (m_players[playerIndex].setWall((y + kMarjaDeEroare), (x + kMarjaDeEroare), m_board, sf::Vector2f(gridSize.x * x + 100, gridSize.y * y + 95), wallTypeIndicator))
						{
							Nod aux1(y + kMarjaDeEroare, x + kMarjaDeEroare);
							Nod aux2(y + kMarjaDeEroare, x + kMarjaDeEroare + 1);
							Nod aux3(y, x + kMarjaDeEroare);
							Nod aux4(y, x + kMarjaDeEroare + 1);
							m_AI.modifiGraf("up", aux1);
							m_AI.modifiGraf("up", aux2);
							m_AI.modifiGraf("down", aux3);
							m_AI.modifiGraf("down", aux4);
							++wallIndex;
							if (playerIndex < 1)
								++playerIndex;
							else
								--playerIndex;
							return;
						}
						else std::cout << "This move iz not gud !\n";
						break;
					}
					catch (const std::out_of_range)
					{
						std::cout << "This move iz not gud !\n";
						break;
					}

				case false:
					try
					{
						if (m_players[playerIndex].setWall((y + kMarjaDeEroare), (x + kMarjaDeEroare), m_board, sf::Vector2f(gridSize.x * x + 100, gridSize.y *y + 95), wallTypeIndicator))
						{
							Nod aux1(y + kMarjaDeEroare, x + kMarjaDeEroare);
							Nod aux2(y , x + kMarjaDeEroare);
							Nod aux3(y + kMarjaDeEroare, x + kMarjaDeEroare + 1);
							Nod aux4(y , x + kMarjaDeEroare + 1);
							m_AI.modifiGraf("right", aux1);
							m_AI.modifiGraf("right", aux2);
							m_AI.modifiGraf("left", aux3);
							m_AI.modifiGraf("left", aux4);
							++wallIndex;
							if (playerIndex < 1)
								++playerIndex;
							else
								--playerIndex;
							return;
						}
						else std::cout << "This move iz not gud !\n";
						break;

					}
					catch (const  std::out_of_range)
					{

						std::cout << "This move iz not gud !\n";
						break;
					}
				}
			}
		}
		windowFor2(std::forward<sf::RenderWindow&>(Window),playerIndex);
	}
}

void GameGraphics::verticalWallPlace(const float& x, const float& y, int & index)
{
	Logg::loggFunction("This method will set a vertical wall on board, depending on coordinates. \n");

	++index;
	m_wallVector[index].setPosition(gridSize.x * x + 100, gridSize.y * y + 95);

}

void GameGraphics::horizontalWallPlace(const float& x, const float& y, int & index)
{
	Logg::loggFunction("This method will set a horizontal wall on board, depending on coordinates. \n");

	++index;
	m_wallVector[index].setPosition(gridSize.x * x + 100, gridSize.y * y + 95);
	m_wallVector[index].rotate(90.0f);

}


void GameGraphics::buildingPieces(sf::Texture& texture)
{

	sf::Vector2u textureSize = texture.getSize();
	sf::RectangleShape piece(sf::Vector2f(textureSize.x - 10, textureSize.y - 10));
	piece.setTexture(&texture);
	m_pieces.push_back(piece);
}




