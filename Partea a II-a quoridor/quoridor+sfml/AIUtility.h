#pragma once
#include "Nod.h"
#include"Board.h"
#include <vector>
#include <list>
#include <queue>
using namespace std;

class AIUtility
{
public:
	AIUtility();

	void createGraf(const Board<char> &);
	void print() const;
	void modifiGraf(const string&, const Nod&);
	void hinturi(const pair<int, int>&, const pair<int, int>, const bool&)const;
private:
	void minim(int &, int&, const vector<int>&) const;
	void bFS(const std::pair<int, int>&, vector<int>&, vector<int>&) const;
	int returnIndex(Nod&) const;
	bool verificareNevizitate(Nod&, vector<Nod>&) const;
private:
	vector<Nod> m_graf;
	vector<Nod> m_pozFinaleP1;
	vector<Nod> m_pozFinaleP2;
};

