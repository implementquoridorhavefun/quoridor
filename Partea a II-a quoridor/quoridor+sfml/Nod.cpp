#include "pch.h"
#include "Nod.h"


Nod::Nod()
{
}

Nod::Nod(const int& linie,const int& coloana) 
{
	m_indiceNod = make_pair(linie, coloana);
}

Nod::Nod(const Nod & other)
{
	*this = other;
}

Nod::Nod(Nod && other)
{
	*this = move(other);
}

void Nod::addVecini(string pozitieVecin, Nod vecin)
{
	m_vecini.emplace(pozitieVecin, vecin);
}

void Nod::deleteVecin(string pozitieVecin)
{
	for (auto it = m_vecini.begin(); it != m_vecini.end();)
	{
		if (it->first == pozitieVecin)
		{
			m_vecini.erase(it);
			break;
		}
		else
			it++;
	}
}

int Nod::getFirst() const
{
	return m_indiceNod.first;
}

int Nod::getSecond() const
{
	return m_indiceNod.second;
}

vector<Nod> Nod::getVecini() const
{
	vector<Nod> vecini;
	for (auto it : m_vecini)
		vecini.push_back(it.second);
	return vecini;
}

bool Nod::operator==(const Nod & other) const
{
	if (m_indiceNod == other.m_indiceNod)
		return true;
	else return false;
}

Nod & Nod::operator=(const Nod & other)
{
	m_indiceNod = other.m_indiceNod;
	m_vecini = other.m_vecini;

	return *this;
}

Nod & Nod::operator=(Nod && other)
{
	m_indiceNod = other.m_indiceNod;
	m_vecini = other.m_vecini;

	new(&other) Nod;
	 
	return *this;
}


std::ostream & operator<<(std::ostream & os, const Nod & nod)
{
	os << nod.m_indiceNod.first << " " << nod.m_indiceNod.second<<" \n";
	for (auto it : nod.m_vecini)
	{
		os << it.first <<" "<< it.second <<"\n";
	}
	return os;
}
