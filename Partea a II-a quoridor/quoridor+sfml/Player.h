
#pragma once


#include "Board.h"
#include "../NewDll/PawnDll.h"

#include <SFML\Graphics.hpp>
#include <iostream>
#include <string>
#include<memory>
class Player
{
public:

	Player(const std::shared_ptr<std::string> name, const uint8_t& availableWalls, const uint8_t& xCoord, const uint8_t& yCoord);

	bool setWall(const uint8_t&, const uint8_t&, Board<char>& board, sf::Vector2f origin, const bool& wallType);

	bool movePawn( Board<char>& board, std::vector<std::pair<int,int>>& pawnPositions,bool &flag,const int& playerIndex);


	bool checkIfWeHaveAWinner(const int& playerIndex);

	int getWallNr()const;

	std::pair<int, int> getXY() const ;

	std::string getName() const;

private:
	std::shared_ptr<std::string> m_name;
	uint8_t m_availabeWalls;
	Pawn m_pion;
};

