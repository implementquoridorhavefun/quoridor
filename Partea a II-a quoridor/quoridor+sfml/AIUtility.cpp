#include "pch.h"
#include "AIUtility.h"

const bool P1 = true;
const bool P2 = false;

AIUtility::AIUtility()
{
}

void AIUtility::createGraf(const Board<char>& board)
{
	for (int i = 1; i < 10; i++)
		for (int j = 1; j < 10; j++)
		{
			Nod n(i, j);
			if (!board.hasUpperWall(i, j))
			{
				Nod aux(i - 1, j);
				n.addVecini("up", aux);
			}
			if (!board.hasDownWall(i, j))
			{
				Nod aux(i + 1, j);
				n.addVecini("down", aux);
			}
			if (!board.hasRightWall(i, j))
			{
				Nod aux(i, j + 1);
				n.addVecini("right", aux);
			}
			if (!board.hasLeftWall(i, j))
			{
				Nod aux(i, j - 1);
				n.addVecini("left", aux);
			}
			m_graf.push_back(n);
		}
	for (int i = 1; i < 10; i++)
	{
		Nod nod(9, i);
		m_pozFinaleP1.push_back(nod);
	}
	for (int i = 1; i < 10; i++)
	{
		{
			Nod nod(1, i);
			m_pozFinaleP2.push_back(nod);
		}
	}
}

void AIUtility::print() const
{
	for (auto a : m_graf)
	{
		cout << a;
	}
}

void AIUtility::modifiGraf(const string & pozitieVecin, const Nod& nod)
{
	auto it = find(m_graf.begin(), m_graf.end(), nod);
	it->deleteVecin(pozitieVecin);
}

void AIUtility::minim(int& min, int & index,const vector<int>& lungimeDrum )const
{
     min = INT_MAX;
	for (int i = 0; i < lungimeDrum.size(); i++)
	{
		if (lungimeDrum[i] < min)
		{
			min = lungimeDrum[i];
			index = i;
		}
	}
}

void AIUtility::bFS(const std::pair<int, int>& coordonate,vector<int>& predecesori,vector<int>& lungime) const
{
	vector <Nod> u; // noduri nevizitate
	queue <Nod> v; // noduri vizitate si neanalizate
	vector <Nod> w; // noduri vizitate si analizte 

	Nod aux;
	for (Nod nod : m_graf)
	{
		predecesori.emplace_back(0);
		lungime.push_back(0);
		u.push_back(nod);
	}

	Nod start(coordonate.first, coordonate.second);
	auto it = find(u.begin(), u.end(), start);
	v.push(*it);
	u.erase(it);
	predecesori[returnIndex(start)]=0;
	lungime[returnIndex(start)] = 0;
	while (!v.empty())
	{
		aux = v.front();
		bool flag = true;
		for (Nod vecni : aux.getVecini())
		{
			if (verificareNevizitate(vecni, u))
			{
				auto it = find(u.begin(), u.end(), vecni);
				v.push(*it);
				predecesori[returnIndex(vecni)] = returnIndex(aux);
				lungime[returnIndex(vecni)] = lungime[returnIndex(aux)] + 1;
				u.erase(it);
			}
			else flag = false;
		}
		if (flag == false)
		{
			w.push_back(aux);
			v.pop();
		}
	}
}

void AIUtility::hinturi(const pair<int, int>& playerCurent, const pair<int, int> playerSecund, const bool& flagPlayer) const
{
	vector <int> lungimeDrumPCurent;
	vector<vector<Nod>> posibileDrumuriPCurent;
	vector<int> predecesoriPCurent;
	vector <int> lungimePCurent;

	vector <int> lungimeDrumPSecund;
	vector<vector<Nod>> posibileDrumuriPSecund;
	vector<int> predecesoriPSecund;
	vector<int> lungimePSecund;

	bFS(playerCurent, predecesoriPCurent, lungimePCurent);
	bFS(playerSecund, predecesoriPSecund, lungimePSecund);

	vector<Nod> drum;
	Nod start(playerCurent.first, playerCurent.second);
	Nod secund(playerSecund.first, playerSecund.second); //repara

	string mesaj;
	if (flagPlayer == P1)
	{
		for (auto nod : m_pozFinaleP1)
		{
			auto index = returnIndex(nod);
			lungimeDrumPCurent.push_back(lungimePCurent[index]);
			auto aux = nod;
			while (!(aux == start))
			{
				drum.push_back(aux);
				aux = m_graf[predecesoriPCurent[returnIndex(aux)]];
			}
			posibileDrumuriPCurent.push_back(drum);
			drum.clear();
		}
		for (auto nod : m_pozFinaleP2)
		{
			auto index = returnIndex(nod);
			lungimeDrumPSecund.push_back(lungimePSecund[index]);
			auto aux = nod;
			while (!(aux == secund))
			{
				drum.push_back(aux);
				aux = m_graf[predecesoriPSecund[returnIndex(aux)]];
			}
			posibileDrumuriPSecund.push_back(drum);
			drum.clear();
		}
	}
	else
	{
		for (auto nod : m_pozFinaleP2)
		{
			auto index = returnIndex(nod);
			lungimeDrumPCurent.push_back(lungimePCurent[index]);
			auto aux = nod;
			while (!(aux == start))
			{
				drum.push_back(aux);
				aux = m_graf[predecesoriPCurent[returnIndex(aux)]];
			}
			posibileDrumuriPCurent.push_back(drum);
			drum.clear();
		}
		for (auto nod : m_pozFinaleP1)
		{
			auto index = returnIndex(nod);
			lungimeDrumPSecund.push_back(lungimePSecund[index]);
			auto aux = nod;
			while (!(aux == secund))
			{
				drum.push_back(aux);
				aux = m_graf[predecesoriPSecund[returnIndex(aux)]];
			}
			posibileDrumuriPSecund.push_back(drum);
			drum.clear();
		}
	}
	 
	int indexPCurent;
	int minimPCurent;

	int indexPSecund;
	int minimPSecund;

	minim(minimPCurent, indexPCurent, lungimeDrumPCurent);
	minim(minimPSecund, indexPSecund, lungimeDrumPSecund);
	
	if (minimPCurent > minimPSecund)
	{
		vector<Nod> celMaiBunDrum = posibileDrumuriPSecund[indexPSecund];
		Nod nodSecund = celMaiBunDrum[celMaiBunDrum.size() - 1];

		cout << "Your oponent has a " <<  minimPCurent-minimPSecund  << " moves advantage .\n";
		cout << "Try to place a wall in " << nodSecund.getFirst() <<" "<< nodSecund.getSecond() << "\n";
	}
	else
	{
		vector<Nod> celMaiBunDrum = posibileDrumuriPCurent[indexPCurent];
		Nod nodCurent = celMaiBunDrum[celMaiBunDrum.size() - 1];
		
		cout << " You have a " << minimPSecund - minimPCurent << "turn advantage \n";
		cout << " Try to move to " << nodCurent.getFirst() << " " << nodCurent.getSecond() << " to keep it that way\n ";
	}
}

int AIUtility::returnIndex( Nod& nod) const
{
	for (int i = 0; i < m_graf.size(); i++)
	{
		if (nod == m_graf[i])
			return i;
	}
	return -1;
}

bool AIUtility::verificareNevizitate(Nod & nod, vector<Nod>& nevizitate) const
{
	if (find(nevizitate.begin(), nevizitate.end(), nod) != nevizitate.end())
		return true;
	else
		return false;
}




