#pragma once
#include "pch.h"

#include <iostream>

class Pawn
{
public:
	enum class Color : uint8_t
	{
		Red,
		Blue,
		Green,
		Yellow
	};
public:
	Pawn();
	Pawn(Color color);
	Pawn& operator=(const Pawn& other);
	~Pawn();

	Color GetColor() const;

	friend std::ostream& operator << (std::ostream& os, const Pawn& pawn);


	int GetCoordX() const;
	int GetCoordY() const;

	void SetCoordX(const float& pawn);
	void SetCoordY(const float& pawn);


private:
	Color m_color;

	int m_coordX;
	int m_coordY;
};

