
#include "pch.h"
#pragma once


#include "Board.h"
#include "Pawn.h"
#include <iostream>
#include <string>
class Player
{
public:

	Player(const std::string& name, const uint8_t& availableWalls);

	void SetWall(std::istream& in, Board& board) const;
	//coordonatele de inceput ale pionului sunt standard 
	//mutarea stanga,dreapta,sus,jos se vor face utilizand functiile de la tastatura 
	//cu verificarea daca mutara este pozibila
	//se va folosii sfml
	void MovePawn(std::istream& in, Board& board)const;
	
private :
	std::string m_name;
	uint8_t m_availabeWalls;
	Pawn m_pion;
};

