#include "Pawn.h"
#include"pch.h"

Pawn::Pawn(Color color) :
	m_color(color)
{

}

Pawn::Color Pawn::GetColor() const
{
	return m_color;
}

Pawn::Pawn()
{
}

Pawn& Pawn::operator=(const Pawn& other)
{
	m_color = other.m_color;
	return *this;
}

Pawn::~Pawn()
{
}

std::ostream& operator<<(std::ostream& os, const Pawn& pawn)
{
	return os << pawn.m_color;
}

 float Pawn::GetCoordX() const
{
	 return m_coordX;
}

 float Pawn::GetCoordY() const
 {
	 return m_coordY;
 }

 void Pawn::SetCoordX(const Pawn& pawn)
 {
	 this->m_coordX = pawn.m_coordX;
 }

 void Pawn::SetCoordY(const Pawn& pawn)
 {
	 this->m_coordY = pawn.m_coordY;
 }