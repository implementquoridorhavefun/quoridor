#pragma once
#include "pch.h"

#include <iostream>

class Pawn
{
public:
	enum class Color : uint8_t
	{
		Red,
		Blue,
		Green,
		Yellow
	};
public:
	Pawn();
	Pawn(Color color);
	Pawn& operator=(const Pawn& other);
	~Pawn();

	Color GetColor() const;

	friend std::ostream& operator << (std::ostream& os, const Pawn& pawn);

private:
	float GetCoordX() const;
	float GetCoordY() const;

	void SetCoordX(const Pawn& pawn);
	void SetCoordY(const Pawn& pawn);


private:
	Color m_color;

	float m_coordX;
	float m_coordY;
};

