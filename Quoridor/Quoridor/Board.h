#pragma once
#include "pch.h"

#include <iostream>

class Board
{
public:
	Board();
	bool IsSpeaceForHorizontalWall(const int x,const int y)const;
	bool IsSpeaceForVerticalWall(const int x, const int y)const;
	void SetVerticalWallToCell(const int x, const int y);
	void SetHorizontalWallToCell(const int x, const int y);
	bool HasUpperWall(const int x, const int y)const;
	bool HasDownWall(const int x, const int y)const;
	bool HasRightWall(const int x, const int y)const;
	bool HasLeftWall(const int x, const int y)const;
	void SetUpCorner(const int x, const int y);
	void SetDownCorner(const int x, const int y);
	void SetRightCorner(const int x, const int y);
	void SetLeftCorner(const int x, const int y);
	bool IsUpCornerSetted(const int x,const int y)const;
	bool IsDownCornerSetted(const int x, const int y)const;
	bool IsRightCornerSetted(const int x, const int y)const;
	bool IsLeftCornerSetted(const int x, const int y)const;
	//int get_player(int x, int y);

	friend std::ostream& operator <<(std::ostream& os, Board board);
private:
	bool IsOutOfBounds(const int x, const int y)const;
private:
	static const int bounds = 9;
	static const int up = 0;
	static const int right = 1;
	static const int down = 2;
	static const int left = 3;
	static const int upCorner = 4;
	static const int rightCorner = 5;
	static const int downCorner = 6;
	static const int leftCorner = 7;
    char board[10][10];

};

