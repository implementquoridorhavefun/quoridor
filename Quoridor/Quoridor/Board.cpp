#include "Board.h"

#include"pch.h"


Board::Board()
{
}

bool Board::IsSpeaceForHorizontalWall(const int x,const int y)const
{
	if (HasUpperWall(x, y) == false && HasUpperWall(x, y + 1) == false && HasDownWall(x - 1, y) == false && HasDownWall(x - 1, y + 1)==false)
		return true;
	return false;
}

bool Board::IsSpeaceForVerticalWall(const int x, const int y)const
{
	if (HasLeftWall(x, y) == false && HasRightWall(x, y + 1) == false && HasLeftWall(x - 1, y) == false && HasRightWall(x - 1, y + 1)==false)
		return true;
	return false;
}

bool Board::IsOutOfBounds(const int x, const int y) const
{
	if (x < 0 || x > bounds || y < 0 || y > bounds)
		return true;
	return false;
}

bool Board::HasUpperWall(const int x, const int y)const
{
	if (this->board[x][y] & (1 << up)) return true;
	return false;
}

bool Board::HasDownWall(const int x, const int y) const
{
	if (this->board[x][y] & (1 << down)) return true;
	return false;
}

bool Board::HasRightWall(const int x, const int y)const
{
	if (this->board[x][y] & (1 << right)) return true;
	return false;
}

bool Board::HasLeftWall(const int x, const int y)const
{
	if (this->board[x][y] & (1 << left)) return true;
	return false;
}
void Board::SetUpCorner(const int x, const int y)
{
	this->board[x][y] | (1 << upCorner);
}
void Board::SetDownCorner(const int x, const int y)
{
	this->board[x][y] | (1 << downCorner);
}
void Board::SetRightCorner(const int x, const int y)
{
	this->board[x][y] | (1 << rightCorner);
}
void Board::SetLeftCorner(const int x, const int y)
{
	this->board[x][y] | (1 << leftCorner);
}
bool Board::IsUpCornerSetted(const int x, const int y) const
{
	if (this->board[x][y] & (1 << upCorner)) return true;
	return false;
}
bool Board::IsDownCornerSetted(const int x, const int y) const
{
	if (this->board[x][y] & (1 << downCorner)) return true;
	return false;
}
bool Board::IsRightCornerSetted(const int x, const int y) const
{
	if (this->board[x][y] & (1 <<rightCorner)) return true;
	return false;
}
bool Board::IsLeftCornerSetted(const int x, const int y) const
{
	if (this->board[x][y] & (1 << leftCorner)) return true;
	return false;
}
void Board::SetVerticalWallToCell(const int x, const int y)
{
	if (IsOutOfBounds(x - 1, y + 1) == false )
	{
		this->board[x][y] |= (1 << right);
		this->board[x - 1][y] |= (1 << right);
		this->board[x][y + 1] |= (1 << left);
		this->board[x - 1][y + 1] |= (1 << left);
	}
}

void Board::SetHorizontalWallToCell(const int x, const int y)
{
	if (IsOutOfBounds(x - 1, y + 1) == false )
	{
		this->board[x][y] |= (1 << up);
		this->board[x - 1][y] |= (1 << down);
		this->board[x][y + 1] |= (1 << up);
		this->board[x - 1][y + 1] |= (1 << down);
	}
}



//int Board::get_player(int x, int y)
//{
//
//	if (this->board[x][y] & (1 << player_1)) return 1;
//	if (this->board[x][y] & (1 << player_2)) return 2;
//	if (this->board[x][y] & (1 << player_3)) return 3;
//	if (this->board[x][y] & (1 << player_4)) return 4;
//	return 0;
//
//}

std::ostream & operator<<(std::ostream & os, Board board)
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
			os << board.board[i][j]<<" ";
		os << " \n";
	}
	return os;
}
